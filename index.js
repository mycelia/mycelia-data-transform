const glob = require("glob");

const methods = {};
glob("./[^.]+.js",{cwd:__dirname+'/src/'},(err,files)=>{
	for(let file of files){
		let name = file.substr(0,file.length() -3);
		mehotds[name] = require(file);
	}
});
module.exports = methods;
