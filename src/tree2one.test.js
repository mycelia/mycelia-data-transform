const app = require('./tree2one');

describe('tree2one',()=>{
	const theOne = "path/to.file";
	it('return a empty file list for an empty input', () => {
		expect(app.tree2one({},theOne)).toEqual({'path/to.file.yml':`{}
`});
	});
	it('use path to place entity', () => {
		expect(app.tree2one({'path/to.file/node/node-plop.yml':`id: node-plop
label: plop`},theOne)).toEqual({'path/to.file.yml':`node:
  - id: node-plop
    label: plop
`});
	});
	it('join multiple entites', () => {
		expect(app.tree2one({'path/to.file/node/node-plop.yml':`id: node-plop
label: plop`, 'path/to.file/node/node-plop2.yml':`id: node-plop2
label: plop2`, 'path/to.file/link/link-plop.yml':`id: link-plop
label: plop`},theOne)).toEqual({'path/to.file.yml':`node:
  - id: node-plop
    label: plop
  - id: node-plop2
    label: plop2
link:
  - id: link-plop
    label: plop
`});
	});
});
