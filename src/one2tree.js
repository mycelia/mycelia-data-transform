const yaml = require('js-yaml');

function one2tree(inputFiles){
	const outputFiles = {};
	for(let path in inputFiles){
		const basePath = path.substr(0,path.lastIndexOf(".yml"))+"/";
		const content = yaml.safeLoad(inputFiles[path]);
		for(let category in content){
			const localPath = basePath+category+"/";
			if(category !== "commitHash") for(let item of content[category]){
				outputFiles[localPath+item.id+'.yml'] = yaml.safeDump(item);
			}
		}
	}
	return outputFiles;
}

module.exports = {one2tree};