const app = require('./one2tree');

describe('one2tree',()=>{
	it('return a empty file list for an empty input', () => {
		expect(app.one2tree({'path/to.file.yml':''})).toEqual({});
	});
	it('ignore commitHash', () => {
		expect(app.one2tree({'path/to.file.yml':`commitHash: osef`})).toEqual({});
	});
	it('return a file for each entity', () => {
		expect(app.one2tree({'path/to.file.yml':`
node:
  - id: node-plop
    label: plop
  - id: node-plop2
    label: plop2
link:
  - id: link-plop
    label: plop
commitHash: osef
`})).toEqual({
'path/to.file/node/node-plop.yml':`id: node-plop
label: plop
`, 'path/to.file/node/node-plop2.yml':`id: node-plop2
label: plop2
`, 'path/to.file/link/link-plop.yml':`id: link-plop
label: plop
` });
	});
});
