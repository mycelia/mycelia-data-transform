const yaml = require('js-yaml');

function tree2one(inputFiles,pathOfTarget){
	const targetStruct = {};
	const basePath = pathOfTarget+"/";
	for(let path in inputFiles){
		const category = path.substring(basePath.length, path.lastIndexOf('/'));
		const content = yaml.safeLoad(inputFiles[path]);
		if(!targetStruct[category]) targetStruct[category] = [];
		targetStruct[category].push(content);
	}
	const outputFiles = {};
	outputFiles[`${pathOfTarget}.yml`]=yaml.safeDump(targetStruct);
	return outputFiles;
}

module.exports = {tree2one};